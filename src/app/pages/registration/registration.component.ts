import { Component, OnInit, ViewChild } from '@angular/core';
import { NouiFormatter } from "ng2-nouislider";
import { config } from 'process';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html'
})
export class RegistrationComponent implements OnInit {
  alertText: string;
  someKeyboard: number = 1;
  someKeyboard2: number = 1;
  someKeyboardConfig: any = {};
  someKeyboardConfig2: any = {};
  detectionTime: number = 30;
  randomColor: string = 'rgb(243, 189, 53)';
  selectedMode: string = 'Accuracy';
  isDisabled: boolean = true;
  compileData: boolean = true;
  fullName: string;

  @ViewChild('sliderRef') sliderRef;

  private someKeyboard2EventHandler = (e: KeyboardEvent) => {
    // determine which handle triggered the event
    //let index = parseInt((<HTMLElement>e.target).getAttribute('data-handle'));
    let multiplier: number = 0;
    let stepSize = 0.5;
  
    switch ( e.which ) {
      case 40:  // ArrowDown
      case 37:  // ArrowLeft
        multiplier = -2;
        e.preventDefault();
        break;
  
      case 38:  // ArrowUp
      case 39:  // ArrowRight
        multiplier = 3;
        e.preventDefault();
        break;
  
      default:
        break;
    }
  
    let delta = multiplier * stepSize;
    let newValue = this.someKeyboard;
    let newValue2 = this.someKeyboard2;
    //newValue[index] += delta;
    this.someKeyboard = newValue;
    this.someKeyboard2 = newValue2;
  }
 
  constructor() { 

    this.someKeyboardConfig = {
      behaviour: 'drag',
      connect: 'lower',
      start: [1],
      keyboard: true,  // same as [keyboard]="true" 
      onKeydown: this.someKeyboard2EventHandler,
      step: 0.5,
      range: {
        min: 1,
        max: 300
      },
      tooltips: [new MyFormatter],
      pips: {
        mode: 'count',
        density: 1,
        values: 6,
        stepped: true,
      }
    };

    this.someKeyboardConfig2 = {
      behaviour: 'drag',
      connect: 'lower',
      start: [1],
      keyboard: true,  // same as [keyboard]="true"
      onKeydown: this.someKeyboard2EventHandler,
      step: 0.5,
      padding: [0, 0],  // number of page steps, defaults to 10
      range: {
        min: 1,
        max: this.detectionTime
      },
      tooltips: [new MyFormatter],
      pips: {
        mode: 'count',
        density: 1,
        values: 6,
        stepped: true
      }
    };
    
  }

  ngOnInit(): void {
    var configuration = JSON.parse(localStorage.getItem('config'));
    if (configuration){ 
      this.fullName = configuration.fullName;
      this.someKeyboard = configuration.cooldown;
      this.someKeyboard2 = configuration.detectTime;
      this.selectedMode = configuration.mode;
    }
  }

  setDetectionTime(event){

      if(event < this.detectionTime) {
        let value = this.detectionTime - event;
        this.sliderRef.slider.updateOptions({
          padding: [0, value]
        });
      }
      else {
        this.sliderRef.slider.updateOptions({
          padding: [0, 0]
        });
      }
  }

  onChange(event) {
    this.setDetectionTime(event);
    this.isDisabled = false;
  }

  activeButton() {
    this.isDisabled = false;
  }

  saveData() {
    this.compileData = false;
    localStorage.setItem('config', JSON.stringify({ fullName: this.fullName, cooldown: this.someKeyboard, detectTime: this.someKeyboard2, mode: this.selectedMode }))
  }

}


//Tooltip Formatter
export class MyFormatter implements NouiFormatter {
  to (value: number): string{
    let output = (Math.round(value*2)/2) + " s";
    return output;
  }

  from(value: string): number {
    return 0;
  }
}
